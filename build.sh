#!/bin/bash

#---------- Variables ----------
# Colors
resetColor="\e[0m\e[0m"
redColor="\e[0;31m\e[1m"
blueColor="\e[0;34m\e[1m"
cyanColor="\e[01;96m\e[1m"
grayColor="\e[0;37m\e[1m"
greenColor="\e[0;32m\e[1m"
magentaColor="\e[0;35m\e[1m"
yellowColor="\e[0;33m\e[1m"
turquoiseColor="\e[0;36m\e[1m"

dot() {
    local symbol=$1
    echo -e "${redColor}[${resetColor}${yellowColor}$symbol${resetColor}${redColor}]${resetColor}"
}

# Trap CTRL + C to exit
trap ctrl_c INT
ctrl_c() {
    echo -e "\n\n$(dot '*')${yellowColor} Exiting...${resetColor}"
    exit
}

#---------- Config ----------
set_default_config() {
    [ -z "$release" ] && release="6.1"
    [ -z "$architecture" ] && architecture="all"
    [ -z "$version" ] && version="lory"
    [ -z "$mirror" ] && mirror="https://deb.parrot.sh/direct/parrot/"
    [ -z "$compress" ] && compress=true
    [ -z "$clean" ] && clean=false
}

ARGUMENT_LIST=(
    "help"
    "release:"
    "architecture:"
    "version:"
    "mirror:"
    "no-compress"
    "clean"
)

# Read arguments
opts=$(getopt --longoptions "$(printf "%s," "${ARGUMENT_LIST[@]}")help" --name "$(basename "$0")" --options "" -- "$@")

# Verify if getopt succeeded
if [ $? -ne 0 ]; then
    echo -e "$(dot '!')${redColor} ERROR: Invalid command-line option${resetColor}"
    exit 1
fi

eval set -- "$opts"
while true; do
    case "$1" in
        --help)
            echo -e "${cyanColor}Parrot OS${resetColor} boostrap builder"
            echo "Usage: $0 [<options>...]"
            echo "Options:"
            for x in "${ARGUMENT_LIST[@]%:}"; do
                echo "  --$x"
            done
            exit 0
            ;;
        --release)
            release=$2
            shift 2
            ;;
        --architecture)
            architecture=$2
            shift 2
            ;;
        --version)
            version=$2
            shift 2
            ;;
        --mirror)
            mirror=$2
            shift 2
            ;;
        --no-compress)
            compress=false
            shift 2
            ;;
        --clean)
            clean=true
            shift 1
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "ERROR: Invalid command-line option"
            exit 1
            ;;
    esac
done
# Default config 
set_default_config


#---------- Script ----------
echo -e "${cyanColor}___  ____ ____ ____ ____ ___    ___  ____ ____ ____ ___ ____ ____ ___  "
echo -e "|__] |__| |__/ |__/ |  |  |     |__] |  | |  | [__   |  |__/ |__| |__] "
echo -e "|    |  | |  \ |  \ |__|  |     |__] |__| |__| ___]  |  |  \ |  | |    ${resetColor}"

# Check requirements
if ! [ -e "/usr/share/debootstrap/scripts/parrot" ]; then
    echo -e "\n$(dot '!')${redColor} This needs to be run on a Parrot system!${resetColor}"
    exit 1
fi

if [ "$(id -u)" -ne 0 ]; then
    echo -e "\n$(dot '!')${redColor} Error: This requires root privileges.${resetColor}"
    exit 1
fi

if ! command -v debootstrap > /dev/null; then
    echo -e "\n$(dot '!')${redColor} The debootstrap is not installed.${resetColor}"
    exit 1
fi

if ! command -v tar > /dev/null; then
    echo -e "\n$(dot '!')${redColor} The tar is not installed.${resetColor}"
    exit 1
fi

if ! command -v xz > /dev/null; then
    echo -e "\n$(dot '!')${redColor} The xz is not installed.${resetColor}"
    exit 1
fi

if ! command -v sha256sum > /dev/null; then
    echo -e "\n$(dot '!')${redColor} The sha256sum is not installed.${resetColor}"
    exit 1
fi

# Set architecture
if [ "$architecture" = "all" ]; then
    architecture="amd64 i386 arm64 armhf"
elif [ "$architecture" = "x64" ]; then
    architecture="amd64"
elif [ "$architecture" = "x86" ]; then
    architecture="i386"
fi
architecture_array=($architecture)

# Build boostrap
mkdir -p boostrap
for arch in "${architecture_array[@]}"; do
    echo -e "\n${blueColor}Building for: $arch$resetColor"
    
    boostrap="boostrap/${version}_${release}_${arch}"
    mkdir -p $boostrap

    debootstrap \
    --arch=$arch \
    --components=main,contrib,non-free,non-free-firmware \
    --include=gnupg2,nano,base-files \
    --exclude=parrot-core \
    $version \
    $boostrap \
    $mirror
done

# Compress
if [ $compress == true ]; then
    mkdir -p images
    
    for arch in "${architecture_array[@]}"; do
        echo -e "\n${blueColor}Compressing for: $arch$resetColor"
        boostrap="boostrap/${version}_${release}_${arch}"
        tar cvfp - $boostrap | xz -q -c --best --extreme - > images/Parrot-boostrap-${release}_${arch}.tar.xz
    done
    
    echo -e "\n${blueColor}Generating hashes...$resetColor"
    sha256sum images/* > sha256-hashes.txt
    echo -e "${blueColor}Printing hashes:$resetColor"
    cat sha256-hashes.txt
fi

# Clean boostrap dirs
if [ $clean == true ]; then
    echo -e "\n${blueColor}Cleaning boostrap dir...$resetColor"
    rm -rfv $boostrap
fi