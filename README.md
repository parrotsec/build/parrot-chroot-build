# Parrot boostrap builder

Usage:
```bash
┌──[parrot@parrot]─[~]
└──╼ $ ./build.sh --help
Parrot OS boostrap builder
Usage: ./build.sh [<options>...]
Options:
  --help
  --release
  --architecture
  --version
  --mirror
  --no-compress
  --clean
```

### Requirements:
- Parrot OS system
- debootstrap (package: debootstrap)
- tar (package: tar)
- xz (package: xz-utils)
- sha256sum (package: coreutils)